# Company Of One - Core

Public information about the thought behind company of one that I learned from a variety of resource including but not limited to GitLab, Zapier, Stripe, Company Of One book, etc.

GitLab values page is one of the most must-read resources about how one best behaves as a working individual in the new world

https://about.gitlab.com/handbook/values

## Remote Work fundamentals
* Tell

If you take a leave, set Google Calendar to Out of office or a simple Slack message in public channel

If you're not at your desk, set Slack status to "Away from desk for <minutes/hours>"

If you're going for lunch or pick up your kids, update your Slack status accordingly


Don't leave your colleagues wondering whether they should expect to have a reply from you soon
